export interface LockDto {
  id: string;
  name: string;
}

export interface KeyDto {
  id: string;
  lockId: string;
  userId: string; // Phone number
  start: Date | null;
  end: Date | null;
  created: Date;
  recurrence?: RecurrenceDto;
}

export interface RecurrenceDto {
  mon?: TimeSpanDto;
  tue?: TimeSpanDto;
  wed?: TimeSpanDto;
  thu?: TimeSpanDto;
  fri?: TimeSpanDto;
  sat?: TimeSpanDto;
  sun?: TimeSpanDto;
}

export interface TimeSpanDto {
  startHour?: number;
  startMinute?: number;
  endHour?: number;
  endMinute?: number;
}
