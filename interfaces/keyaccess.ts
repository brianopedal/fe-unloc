export type KeyAccess = {
  permanent: boolean;
  recurrent: boolean;
};
