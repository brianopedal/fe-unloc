import { KeyDto, LockDto, RecurrenceDto } from "../interfaces/unloc-dtos";
import { faker } from "@faker-js/faker";

const mockIds = [...Array(9)].map(() => faker.database.mongodbObjectId());

export const dummyLocks: LockDto[] = [...Array(9)].map((_, index): LockDto => {
  return {
    id: mockIds[index],
    name: faker.unique(faker.company.bsAdjective),
  };
});

export const mockKeys: KeyDto[] = [...Array(9)].map((_, index): KeyDto => {
  const lockId = mockIds[index];
  let end: Date | null = null;
  let recurrence: RecurrenceDto | undefined = undefined;

  end = handleTempKeyCreation(end, index);
  recurrence = handleRecurringKeyCreation(recurrence, index);

  return {
    id: faker.database.mongodbObjectId(),
    lockId,
    userId: faker.phone.phoneNumber("9## ## ###"),
    start: faker.date.past(),
    end,
    created: faker.date.past(),
    recurrence,
  };
});

function handleTempKeyCreation(end: Date | null, index: number): Date | null {
  if (index > 3 && index < 6) {
    return faker.date.future();
  }
  return end;
}

function handleRecurringKeyCreation(
  recurrence: RecurrenceDto | undefined,
  index: number
): RecurrenceDto | undefined {
  if (index >= 6) {
    const newRecurrence: RecurrenceDto = {
      [faker.date.weekday({ abbr: true })]: {
        startHour: faker.datatype.number({ max: 23, min: 0 }),
        startMinute: faker.datatype.number({ max: 60, min: 0 }),
        endHour: faker.datatype.number({ max: 23, min: 0 }),
        endMinute: faker.datatype.number({ max: 60, min: 0 }),
      },
    };
    return newRecurrence;
  }
  return recurrence;
}
