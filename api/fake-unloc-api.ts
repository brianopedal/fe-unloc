import { dummyLocks, mockKeys } from "./mockdata";
import { KeyDto, LockDto, RecurrenceDto } from "../interfaces/unloc-dtos";

class UnlocFakeApi {
  private locks: LockDto[] = dummyLocks;
  private keys: KeyDto[] = mockKeys;

  getLocks(): LockDto[] {
    return [...this.locks];
  }

  getLock(lockId: string): LockDto | undefined {
    return this.locks.find((lock) => lock.id === lockId);
  }
  getLockByName(name: string): LockDto | undefined {
    return this.locks.find((lock) => lock.name === name);
  }

  getKeysForLock(lockId: string): KeyDto[] {
    return this.keys.filter((k) => k.lockId === lockId);
  }

  getAllKeys(): KeyDto[] {
    console.log("returned new list: " + this.keys.map((k) => k.userId));
    return [...this.keys];
  }

  getKey(id: string): KeyDto | undefined {
    return this.keys.find((k) => k.id === id);
  }

  createKey(
    lockId: string,
    userId: string, // Phone number of the user
    start: Date | null,
    end: Date | null,
    recurrence?: RecurrenceDto
  ): KeyDto {
    const newKey = {
      id: Math.random().toString(36).slice(2, 26),
      lockId: lockId,
      userId: userId,
      start: start,
      end: end,
      created: new Date(),
      recurrence: recurrence,
    };
    this.keys.push(newKey);
    console.log("pushed new key into list: " + newKey.toString());

    return newKey;
  }
}

const unlocApi = new UnlocFakeApi();
export default unlocApi;
