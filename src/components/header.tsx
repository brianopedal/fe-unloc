import { AccountCircle } from "@mui/icons-material";
import { AppBar, Button, Toolbar, Tooltip } from "@mui/material";
import useMediaQuery from "@mui/material/useMediaQuery";

export function Header(): JSX.Element {
  const matches = useMediaQuery("(min-width:600px)");

  return (
    <AppBar position="relative" elevation={0} sx={{ backgroundColor: "white" }}>
      <Toolbar
        style={{
          marginLeft: 0,
          paddingLeft: 0,
          paddingRight: 0,
          paddingBottom: 70,
          paddingTop: 20,
          justifyContent: "fl",
        }}
      >
        <Button>
          <img height={29} width={100} src="unloc.svg" alt="unloc logo" />
          <span
            style={{
              paddingLeft: 20,
              paddingTop: 10,
              fontSize: 27,
              fontWeight: "bold",
              textTransform: "none",
            }}
          >
            KeyManager
          </span>
        </Button>
        {matches && (
          <Tooltip title={<h3>unloc - login placeholder</h3>}>
            <Button sx={{ paddingTop: 2, marginLeft: "auto" }} color="primary">
              <AccountCircle style={{ paddingRight: 5, paddingBottom: 5 }} />
              Login
            </Button>
          </Tooltip>
        )}
      </Toolbar>
    </AppBar>
  );
}

export default Header;
