import { Stack } from "@mui/material";

import { RecurrentDay } from "./recurrent-day";

export function RecurrencentDays({ show = false }): JSX.Element {
  return (
    <Stack justifyContent={"flex-start"} width="100%" spacing={2}>
      {show && (
        <>
          <RecurrentDay dayLabel={"Monday"} value={"mon"} />
          <RecurrentDay dayLabel={"Tuesday"} value={"tue"} />
          <RecurrentDay dayLabel={"Wednesday"} value={"wed"} />
          <RecurrentDay dayLabel={"Thursday"} value={"thu"} />
          <RecurrentDay dayLabel={"Friday"} value={"fri"} />
          <RecurrentDay dayLabel={"Saturday"} value={"sat"} />
          <RecurrentDay dayLabel={"Sunday"} value={"sun"} />
        </>
      )}
    </Stack>
  );
}

export default RecurrencentDays;
