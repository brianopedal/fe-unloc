import { Autocomplete, TextField } from "@mui/material";
import { LockDto } from "../../../interfaces/unloc-dtos";

interface LockChoiceProps {
  handleLockid: (value: string | null) => void;
  locks: LockDto[];
}

export function LockChoice({ handleLockid, locks }: LockChoiceProps): JSX.Element {
  return (
    <Autocomplete
      id="lock-autocomplete"
      onChange={(_, value) => handleLockid(value)}
      options={locks.map((lock) => lock.name)}
      renderInput={(params) => (
        <TextField
          sx={{ maxWidth: 228 }}
          {...params}
          label="Attach to lock"
          InputProps={{
            ...params.InputProps,
            type: "search",
          }}
        />
      )}
    />
  );
}

export default LockChoice;
