import { ChangeEvent, useContext, useEffect, useState } from "react";
import { FormControlLabel, Grid, TextField, Checkbox } from "@mui/material";
import { LocalizationProvider, TimePicker } from "@mui/x-date-pickers";
import { AdapterDateFns } from "@mui/x-date-pickers/AdapterDateFns";
import { fromTimeBase, toTimeBase } from "../../utils/time-utilities";
import { IAppContext, AppCtx } from "../../utils/appcontext";
import { TimeSpanDto } from "../../../interfaces/unloc-dtos";

interface RecurrentDayProps {
  dayLabel: string;
  value: string;
}

export function RecurrentDay({ dayLabel, value }: RecurrentDayProps): JSX.Element {
  const [checked, setChecked] = useState(false);
  const [fromTime, setFromTime] = useState<Date | null>(fromTimeBase);
  const [toTime, setToTime] = useState<Date | null>(toTimeBase);

  const { recurrence, setRecurrence } = useContext<IAppContext>(AppCtx);

  const handleChange = (event: ChangeEvent<HTMLInputElement>) => {
    setChecked(event.target.checked);
  };

  useEffect(() => {
    setRecurrence({
      ...recurrence,
      [value]: {
        startHour: fromTime?.getHours(),
        startMinute: fromTime?.getMinutes(),
        endHour: toTime?.getHours(),
        endMinute: toTime?.getMinutes(),
      } as TimeSpanDto,
    });
    // TODO: remove
    // console.log(`value is now: ${value}`);
    // console.log(`${fromTime?.getHours()} : ${fromTime?.getMinutes()}`);
    // console.log(`${toTime?.getHours()} : ${toTime?.getMinutes()}`);

    if (!checked) {
      // reccurrence day clear on uncheck
      console.log(`clear ${value}: `);
      setRecurrence({
        ...recurrence,
        [value]: undefined,
      });

      // TODO: remove
      // console.log(`value is now: ${value}`);
      // console.log(`${fromTime?.getHours()} : ${fromTime?.getMinutes()}`);
      // console.log(`${toTime?.getHours()} : ${toTime?.getMinutes()}`);
    }
    return () => {
      setRecurrence({
        ...recurrence,
        [value]: undefined,
      });
    };
  }, [checked, fromTime, toTime]);

  return (
    <Grid container spacing={1} display="flex" paddingTop={2} marginLeft={0}>
      <Grid item sx={{ paddingLeft: 0, marginLeft: 0 }}>
        <FormControlLabel
          sx={{ padding: 0.5, width: 95, color: checked ? "black" : "lightGrey" }}
          style={{ justifyContent: "flex-start" }}
          control={
            <Checkbox
              checked={checked}
              onChange={handleChange}
              sx={{ marginLeft: 0, paddingLeft: 0 }}
            />
          }
          label={dayLabel}
          labelPlacement="end"
          value={value}
        />
      </Grid>

      <Grid item xs={4} style={{ marginRight: 0 }}>
        <LocalizationProvider dateAdapter={AdapterDateFns}>
          <TimePicker
            ampm={false}
            disabled={!checked}
            label={checked ? "From" : ""}
            renderInput={(params) => <TextField {...params} />}
            value={fromTime}
            onChange={(newFromTime) => setFromTime(newFromTime)}
          />
        </LocalizationProvider>
      </Grid>
      <Grid item xs={4}>
        <LocalizationProvider dateAdapter={AdapterDateFns}>
          <TimePicker
            ampm={false}
            disabled={!checked}
            label={checked ? "To" : ""}
            renderInput={(params) => <TextField {...params} />}
            value={toTime}
            onChange={(newToTime) => setToTime(newToTime)}
          />
        </LocalizationProvider>
      </Grid>
    </Grid>
  );
}

export default RecurrentDay;
