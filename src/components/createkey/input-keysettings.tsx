import { FormControl, FormControlLabel, FormGroup, FormLabel, Switch } from "@mui/material";
import { useState, ChangeEvent } from "react";

type KeyAccess = {
  permanent: boolean;
  recurrent: boolean;
};

interface KeyAccessSwitchGroupProp {
  access: KeyAccess;
  setAccess: (value: KeyAccess) => void;
}

export function KeyAccessSwitchGroup(prop: KeyAccessSwitchGroupProp): JSX.Element {
  const { access, setAccess } = prop;

  const handleChange = (event: ChangeEvent<HTMLInputElement>) => {
    setAccess({
      ...access,
      [event.target.name]: event.target.checked,
    });
  };

  return (
    <FormControl component="fieldset" variant="standard">
      <FormLabel component="legend">Key Access Settings</FormLabel>
      <FormGroup>
        <FormControlLabel
          control={<Switch checked={access.permanent} onChange={handleChange} name="permanent" />}
          label="Permanent Access"
        />
        <FormControlLabel
          control={<Switch checked={access.recurrent} onChange={handleChange} name="recurrent" />}
          label="Recurrent Access"
        />
      </FormGroup>
    </FormControl>
  );
}

export default KeyAccessSwitchGroup;
