import { Dispatch, SetStateAction } from "react";
import { Container, Stack, TextField } from "@mui/material";

import KeyDatePicker from "./input-datepicker";
import KeyAccessSwitchGroup from "./input-keysettings";
import RecurrencentDays from "./recurrent-days";
import unlocApi from "../../../api/fake-unloc-api";
import LockChoice from "./input-lock";
import { KeyAccess } from "../../../interfaces/keyaccess";
import { LockDto } from "../../../interfaces/unloc-dtos";

interface KeyInputFormProps {
  keyAccess: KeyAccess;
  setKeyAccess: Dispatch<SetStateAction<KeyAccess>>;
  enableDate: Date | null;
  setEnableDate: Dispatch<SetStateAction<Date | null>>;
  disableDate: Date | null;
  setDisableDate: Dispatch<SetStateAction<Date | null>>;
  setLockId: Dispatch<SetStateAction<string>>;
  userId: string;
  setUserId: Dispatch<SetStateAction<string>>;
  allLocks: LockDto[];
}

export function KeyInputForm(props: KeyInputFormProps): JSX.Element {
  const {
    keyAccess,
    setKeyAccess,
    enableDate,
    setEnableDate,
    disableDate,
    setDisableDate,
    setLockId,
    userId,
    setUserId,
    allLocks,
  } = props;

  const setLockIdByName = (lockname: string | null): void => {
    const lock = allLocks.find((l) => l.name === lockname);
    if (!lock) return;
    setLockId(lock.id);
  };

  const showTo = !keyAccess.permanent;

  return (
    <Container sx={{ marginTop: 5 }}>
      <Stack spacing={2.5} sx={{ maxWidth: 800 }}>
        <KeyAccessSwitchGroup access={keyAccess} setAccess={setKeyAccess} />
        <TextField
          sx={{ maxWidth: 230 }}
          required
          label="Phone"
          value={userId}
          onChange={(e) => setUserId(e.target.value)}
        />
        <LockChoice handleLockid={setLockIdByName} locks={unlocApi.getLocks()} />
        <KeyDatePicker date={enableDate} setDate={setEnableDate} label="Activation date" />
        {showTo && (
          <KeyDatePicker date={disableDate} setDate={setDisableDate} label="Deactivation date" />
        )}
        <RecurrencentDays show={keyAccess.recurrent} />
      </Stack>
    </Container>
  );
}

export default KeyInputForm;
