import { DatePicker, LocalizationProvider } from "@mui/x-date-pickers";
import { AdapterDateFns } from "@mui/x-date-pickers/AdapterDateFns";
import { Box, TextField } from "@mui/material";

interface KeyDatePickerProp {
  date: Date | null;
  setDate: (e: Date | null) => void;
  label: string;
}

export function KeyDatePicker({ date, setDate, label }: KeyDatePickerProp): JSX.Element {
  return (
    <Box>
      <LocalizationProvider dateAdapter={AdapterDateFns}>
        <DatePicker
          label={label}
          value={date}
          onChange={(newDate) => setDate(newDate)}
          renderInput={(params) => <TextField {...params} />}
        />
      </LocalizationProvider>
    </Box>
  );
}

export default KeyDatePicker;
