import { Button } from "@mui/material";

export function OpenDialogButton({ openDialog }: { openDialog: (value: boolean) => void }) {
  return (
    <Button
      variant="contained"
      color="primary"
      onClick={() => openDialog(true)}
      sx={{ marginBottom: 2 }}
    >
      Create Key
    </Button>
  );
}

export default OpenDialogButton;
