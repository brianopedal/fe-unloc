import { AppBar, Toolbar, IconButton, DialogTitle } from "@mui/material";
import CloseIcon from "@mui/icons-material/Close";
import SaveIcon from "@mui/icons-material/Save";

interface NewKeyBarProps {
  handleCloseDialog: (submit: boolean) => void;
}

export function NewKeyBar({ handleCloseDialog }: NewKeyBarProps) {
  return (
    <AppBar sx={{ position: "relative" }}>
      <Toolbar>
        <IconButton edge="start" color="inherit" onClick={() => handleCloseDialog(false)}>
          <CloseIcon />
        </IconButton>
        <DialogTitle>New Key</DialogTitle>
        <IconButton edge="end" color="inherit" onClick={() => handleCloseDialog(true)}>
          <SaveIcon />
        </IconButton>
      </Toolbar>
    </AppBar>
  );
}

export default NewKeyBar;
