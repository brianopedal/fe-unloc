import unlocApi from "../../api/fake-unloc-api";
import { KeyDto } from "../../interfaces/unloc-dtos";
import { getKeyType } from "../utils/access-utilities";
import {
  Paper,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
} from "@mui/material";

export function KeyTable({ keyList }: { keyList: KeyDto[] }): JSX.Element {
  return (
    <TableContainer component={Paper}>
      <Table sx={{ minWidth: 250 }}>
        <TableHead>
          <TableRow>
            <TableCell>Key (Phone Numbers)</TableCell>
            <TableCell align="right">Lock</TableCell>
            <TableCell align="right">Type</TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {keyList &&
            keyList.map((key) => (
              <TableRow key={key.id} sx={{ "&:last-child td, &:last-child th": { border: 0 } }}>
                <TableCell component="th" scope="row">
                  {key.userId}
                </TableCell>
                <TableCell align="right">{unlocApi.getLock(key.lockId)?.name}</TableCell>
                <TableCell align="right">{getKeyType(key)}</TableCell>
              </TableRow>
            ))}
        </TableBody>
      </Table>
    </TableContainer>
  );
}

export default KeyTable;
