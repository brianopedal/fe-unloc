import { BottomNavigation, BottomNavigationAction, Grid, Stack } from "@mui/material";
import { LinkedIn, YouTube, Instagram, Facebook } from "@mui/icons-material";

export function Footer(): JSX.Element {
  return (
    <Stack paddingTop={5} paddingBottom={5}>
      {/* <h4>If it doesn't open, it's not your door</h4> */}
      <h4>We open doors — for everyone, everywhere.</h4>
      <BottomNavigation>
        <BottomNavigationAction
          sx={{ marginLeft: 0, paddingLeft: 0 }}
          label="linkedin"
          icon={<LinkedIn />}
          href="https://www.linkedin.com/company/unlocapp"
          target={"_blank"}
        />
        <BottomNavigationAction
          label="youtube"
          icon={<YouTube />}
          href="https://www.youtube.com/channel/UClWcqldvglVFXEIo3G0sMOA"
          target={"_blank"}
        />
        <BottomNavigationAction
          label="instagram"
          icon={<Instagram />}
          href="https://www.instagram.com/unloc.app/"
          target={"_blank"}
        />
        <BottomNavigationAction
          label="facebook"
          icon={<Facebook />}
          href="https://www.facebook.com/unlocapp/"
          target={"_blank"}
        />
      </BottomNavigation>
    </Stack>
  );
}

export default Footer;
