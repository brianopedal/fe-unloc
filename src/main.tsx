import React from "react";
import ReactDOM from "react-dom/client";
import { ThemeProvider } from "@mui/material/styles";
import { theme } from "./utils/theming";
import { Container } from "@mui/system";
import { Footer } from "./components/footer";
import { Header } from "./components/header";
import "./index.css";
import App from "./App";

ReactDOM.createRoot(document.getElementById("root")!).render(
  <React.StrictMode>
    <ThemeProvider theme={theme}>
      <Container>
        <Header />
        <App />
        <Footer />
      </Container>
    </ThemeProvider>
  </React.StrictMode>
);
