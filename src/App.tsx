import unlocApi from "../api/fake-unloc-api";
import { useState } from "react";
import { KeyTable } from "./components/key-table";
import { OpenDialogButton } from "./components/createkey/open-dialogbutton";
import { KeyDto } from "../interfaces/unloc-dtos";
import { IAppContext, AppCtx, appDefaultContext } from "./utils/appcontext";
import { KeyAccess } from "../interfaces/keyaccess";
import { RecurrenceDto } from "../interfaces/unloc-dtos";
import { Dialog } from "@mui/material";
import NewKeyBar from "./components/createkey/bar-newkey";
import KeyInputForm from "./components/createkey/inputform-keydata";

export function App(): JSX.Element {
  const [keyList, setKeyList] = useState<KeyDto[]>(unlocApi.getAllKeys());
  const [openDialog, setOpenDialogue] = useState(false);
  const [keyAccess, setKeyAccess] = useState<KeyAccess>({
    permanent: false,
    recurrent: false,
  });
  const [lockId, setLockId] = useState("");
  const [userId, setUserId] = useState("");
  const [enableDate, setEnableDate] = useState<Date | null>(null);
  const [disableDate, setDisableDate] = useState<Date | null>(null);
  const [recurrenceData, setRecurrenceData] = useState<RecurrenceDto>({});
  const allLocks = unlocApi.getLocks();

  const handleCloseDialog = (save: boolean) => {
    if (save) {
      const start = enableDate;
      let end = null;
      let recurrence = undefined;

      if (!keyAccess.permanent) {
        end = disableDate;
      }
      if (keyAccess.recurrent) {
        recurrence = recurrenceData;
      }

      unlocApi.createKey(lockId, userId, start, end, recurrence);
      setKeyList(unlocApi.getAllKeys());
    } else {
      setLockId("");
      setUserId("");
      setEnableDate(null);
      setDisableDate(null);
      setKeyAccess({ permanent: false, recurrent: false });
    }
    setOpenDialogue(false);
  };

  const appContext: IAppContext = { ...appDefaultContext };

  return (
    <>
      <OpenDialogButton openDialog={setOpenDialogue} />
      <Dialog fullScreen open={openDialog}>
        <NewKeyBar handleCloseDialog={handleCloseDialog} />
        <AppCtx.Provider value={appContext}>
          <KeyInputForm
            keyAccess={keyAccess}
            setKeyAccess={setKeyAccess}
            enableDate={enableDate}
            setEnableDate={setEnableDate}
            disableDate={disableDate}
            setDisableDate={setDisableDate}
            setLockId={setLockId}
            userId={userId}
            setUserId={setUserId}
            allLocks={allLocks}
          />
        </AppCtx.Provider>
      </Dialog>
      <KeyTable keyList={keyList} />
    </>
  );
}

export default App;
