import { createContext, Dispatch, SetStateAction } from "react";
import { RecurrenceDto } from "../../interfaces/unloc-dtos";

export interface IAppContext {
  recurrence: RecurrenceDto;
  setRecurrence: Dispatch<SetStateAction<RecurrenceDto>>;
}

export const appDefaultContext: IAppContext = {
  recurrence: {},
  setRecurrence: () => {},
};

export const AppCtx = createContext({} as IAppContext);
