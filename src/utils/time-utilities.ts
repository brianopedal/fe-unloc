const unixYear = 1970;
const unixMonth = 1;
const unixDay = 1;

export const fromTimeBase = new Date(unixYear, unixMonth, unixDay, 0, 0, 0, 0);
export const toTimeBase = new Date(unixYear, unixMonth, unixDay, 23, 59, 0, 0);
