import { createTheme } from "@mui/material/styles";

export const theme = createTheme({
  palette: {
    primary: {
      main: "#376E26",
    },
    secondary: {
      main: "#C4EE4D",
    },
    text: {
      disabled: "#AAAAAA",
      primary: "#112A09",
      secondary: "#AAAAAA",
    },
  },
});
