import type { KeyDto } from "../../interfaces/unloc-dtos";

export type KeyType = "Permanent" | "Temporary" | "Recurring" | null;

export const getKeyType = (key: KeyDto): KeyType => {
  if (key.recurrence) return "Recurring";
  if (key.start && key.end) return "Temporary";
  if (key.start) return "Permanent";
  return null;
};
