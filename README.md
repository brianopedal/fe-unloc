# 🔓 Interview case - Frontend developer

Simple web app sketch enabling unloc user to create keys to owned locks.

- The App is published at: **https://brianopedal.gitlab.io/fe-unloc/**
- Gitlab Repo is at: **https://gitlab.com/brianopedal/fe-unloc**

## 🚀 Getting started with development

### docker development

1. open in devcontainer with vs code
2. terminal commands:

```
cd fe-uncloc
pnpm dev
```

### If you don't want to use Docker desktop:

1. ensure you have pnpm installed, and use an lts version of NodeJS
2. terminal commands:

```
cd fe-unloc
pnpm install
pnpm dev
```

## 🌀 Assignment Summary

_Features:_

- create keys of different types
  - permanent (start date, no end date)
  - temporary (start & end date)
  - recurring (weekdays and time of day the key will work )
- see all keys with belonging locks and types

**Focus:**

- user interface
- structure
- react skills

**Recuirements:**

- typescript & react
- extendable application
- _if incomplete - ability to explain missing feature implementation_

_Free choices:_

- _UI Library_

_API Support:_

- _getting locks_
- _getting keys_
- _getting keys for a specific lock_
- _creating a key for a lock_

_Assignment Files_

- [Interview case](./docs/case.pdf)
- [api.ts example file](./docs/api.ts)

## 🌟 **unloc** App Description

`Sketch for an unloc keymanager application.`

### 💻 Project status

Status:

You can currently list all keys and create new keys

### 🗺️ Roadmap

- ✅ Add development startup documentation and project information
- ✅ Add Docker devcontainer with extensions in `devcontainer.json` for `Visual Studio Code`
- ✅ Generate mockdata with `fakerJS`
- ✅ Setup templating with Vite JS Tooling and `react-ts`
- ✅ Setup Prettier formatting
- ✅ Setup Vitest with Vitest Extension, and add a canary test
- ✅ Setup Coverage
- ✅ Simple Material UI (MUI) setup
- ✅ Complete `gitlab-ci.yml` file for Gitlab Pages publishing

Not Implemented:

- ❌ Improve mobile first on in general
- ❌ Improve Create Key value state behaviour (clear, remember etc.)
- ❌ Add various input validation and checks for user friendlyness
- ❌ Add aria tags, id's and comply with Universal Design principles
- ❌ Improve chunking to get all prod build chunks below, 500 KiB
- ❌ Extend app theme with more settings.
- ❌ Use global CSS classes for styling, instead of inline where appropriate.
- ❌ Setup fonts with Material UI theme
- ❌ If no testenv for backend in near future, add Mock Service Worker JS or MirageJS to facilitate further development
- ❌ Add production build container

### Application Dependency Graph

![](docs/dependencygraph.png)

## 👨‍🦲 Authors

Brian Opedal
