export class UnlocFakeApi {

    private locks: LockDto[] = []
    private keys: KeyDto[] = []

    constructor() {
        this.locks = [{
            id: 'b8cgy',
            name: 'Ytterdør'
        },
        {
            id: 'a7gbo',
            name: 'Vaskerom'
        }]
    }

    getLocks(): LockDto[] {
        return this.locks    
    }

    getKeysForLock(lockId: string): KeyDto[] {
        return this.keys.filter(k => k.lockId === lockId)
    }

    getAllKeys(): KeyDto[] {
        return this.keys
    }

    getKey(id: string): KeyDto | undefined {
        return this.keys.find(k => k.id === id)
    }

    createKey(
        lockId: string,
        userId: string, // Phone number of the user
        start: Date | null,
        end: Date | null,
        recurrence?: RecurrenceDto
    ): KeyDto {
        const newKey = {
            id: Math.random().toString(36).slice(2, 26),
            lockId: lockId,
            userId: userId,
            start: start,
            end: end,
            created: new Date(),
            recurrence: recurrence
        }
        this.keys.push(newKey)
        return newKey
    }
}

export interface LockDto {
    id: string
    name: string

}

export interface KeyDto {
    id: string
    lockId: string
    userId: string // Phone number
    start: Date | null
    end: Date | null
    created: Date
    recurrence?: RecurrenceDto
}

export interface RecurrenceDto {
    mon?: TimeSpanDto
    tue?: TimeSpanDto
    wed?: TimeSpanDto
    thu?: TimeSpanDto
    fri?: TimeSpanDto
    sat?: TimeSpanDto
    sun?: TimeSpanDto
  }
  
  export interface TimeSpanDto {
    startHour?: number
    startMinute?: number
    endHour?: number
    endMinute?: number
  }